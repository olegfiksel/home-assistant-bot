# Opsdroid home-assistant bot

[![Matrix Channel](https://img.shields.io/matrix/home-assistant-bot:fiksel.info.svg?label=%23home-assistant-bot%3Afiksel.info&logo=matrix&server_fqdn=matrix.fiksel.info)](https://matrix.to/#/#home-assistant-bot:fiksel.info)

Opsdroid bot for triggering home-assistant automations.

![screenshot01](images/screenshot01.png)

The bot reacts to `!` commands.
The commands can be configured using the config file.
Built-in commands:
* `!version`: displays the bot's version

# Video

[![Video](https://i.ytimg.com/vi/7eIHkUdfR-A/maxresdefault.jpg)](https://www.youtube.com/watch?v=7eIHkUdfR-A "Video")

# Running

## docker-compose

* Clone the repository
```
git clone https://gitlab.com/olegfiksel/home-assistant-bot.git
cd home-assistant-bot
```
* Create new config file (copy an example)
```
cp sample-config.yaml configuration.yaml
```
* Adjust the config `configuration.yaml` to your needs
* Create secrets (copy an example)
```
cp secrets-example.env secrets.env
```
* Start the bot
```
docker-compose up -d
docker-compose logs -f
```

# [Howtos](howtos)

## [Wake-up-light](howtos/wake-up-light/README.md)

![](howtos/wake-up-light/showoff.png)
