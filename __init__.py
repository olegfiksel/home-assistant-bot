import logging

from opsdroid.skill import Skill
from opsdroid.message import Message
from opsdroid.matchers import match_regex
from opsdroid_homeassistant import HassSkill

#_LOGGER = logging.getLogger(__name__)

#import pprint
import re
import random

version = "v2.0.1"

#pp = pprint.PrettyPrinter(indent=4)

class Usage(HassSkill):
    def __init__(self, opsdroid, config):
      super(Usage, self).__init__(opsdroid, config)
      # Load custom configuration part
      self.conf = self.opsdroid.config['skills']['home-assistant-bot']['config']
      # Compile all permutations of commands to do the matching futher on
      self.all_commands = {}
      for cmd1 in self.conf['commands']:
        for cmd2 in cmd1['params']:
            compilled_cmd = cmd1['regex'] + r"\s+" + cmd2['param']
            self.all_commands[compilled_cmd] = cmd2

    # Match everthing, what has exclamation mark "!" followed by a word
    @match_regex(r'!\w+', case_sensitive=False, matching_condition="search")
    async def homeassistant(self, message):
        # Debug
        if 'debug' in self.conf and self.conf['debug']:
            await message.respond("I got: `{}`".format(message.text))
        # Print version
        if re.match('^!version', message.text, re.IGNORECASE):
            await message.respond("Version: {}".format(version))

        # Show main usage (all commands)
        if re.search(self.conf['usageWord'], message.text, re.IGNORECASE):
            response = random.choice(['🖐︎ Hi {}!', '🖐︎ Hello {}!', '🖐︎ Hey {}!']).format(message.user)
            response += """
<br>
<b>Commands:</b>
<ul>
"""
            for cmd in self.conf['commands']:
              response += "<li><code>" + cmd['command'] + "</code> - " + cmd['description'] + "</li>"
            response += """
</ul>
"""
            await message.respond(response)

        # Show a certain command usage
        for cmd1 in self.conf['commands']:
            if re.search(cmd1['regex']+"$", message.text, re.IGNORECASE):
                response = """
<b>Usage:</b>
<ul>
"""
                for cmd2 in cmd1['params']:
                    response += "<li><code>" + cmd1['command'] + " " + cmd2['param'] + "</code> - " + cmd2['description'] + "</li>"
                response += """
</ul>
"""
                await message.respond(response)
        
        # Execute command
        for cmd in self.all_commands:
            if re.search(cmd, message.text, re.IGNORECASE):
                item = self.all_commands[cmd]
                response = item['reply']
                ha_domain = item['ha']['domain']
                ha_service = item['ha']['service']
                ha_automation_script = item['ha']['entity_id']
                await message.respond(response)
                await self.call_service(ha_domain, ha_service, entity_id=ha_automation_script)
