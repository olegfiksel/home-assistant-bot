# v2.0.1

* Upgrade dependencies
  * Opsdroid to v0.23.0 and opsdroid-homeassistant to v0.2.0 (to be compotable with opsdroid v0.23.0)

# v2.0.0

* ⚠️  Possible breaking change: change behavior how messages are matched (#4)
  * Before: messages were matched only if a message starts with `!`
  * In this version: the whole message is matched -> this will work `Hey, please !turnon light1`
    * You can override this by using `^` in the config for. For example: `regex: "^!turnon"`
* Update Opsdroid to v0.19.0
* Update opsdroid-homeassistant to 0.1.8
* Add `debug` config parameter to display messages in the chat, that the bot is getting but is not processing/matching

# v1.0.0

* Initial release
